import os
import sys
import time
import textwrap
import subprocess

from PIL import Image
from toga.app import App
from toga.window import MainWindow
from toga.style import Pack
from toga.widgets.divider import Divider
from toga.widgets.label import Label
from toga.widgets.box import Box
from toga.widgets.switch import Switch
from toga.widgets.textinput import TextInput
from toga.widgets.button import Button
from toga.dialogs import OpenFileDialog, InfoDialog, SelectFolderDialog, QuestionDialog
from toga.constants import ROW, COLUMN, CENTER, START, END



class MainForm(App):
    def startup(self):
        self.g_ICON_SIZE = (24, 32, 48, 64, 96, 128) # 各pc平台通配尺寸

        # 分割线
        divider1 = Divider()
        divider2 = Divider()
 
        # 文件选择
        label_discription = Label(text='从文件:', style=Pack(font_size=12))
        self.entry_img_from = TextInput(readonly=True, style=Pack(flex=1))
        button_selection_file = Button(text='...', on_press=self.on_button_select_file)

        box_from_group = Box(style=Pack(direction=ROW, align_items=CENTER, gap=5))
        box_from_group.add(label_discription, self.entry_img_from, button_selection_file)

        # 输出平台选择
        sw_win = Switch('Windows', 'win')
        sw_linux = Switch('Linux', 'linux')
        sw_mac = Switch('mac', 'mac')
        self.g_sw_itmes_widgets = [sw_win, sw_linux, sw_mac]
 
        box_sw_items_group = Box(style=Pack(direction=ROW, gap=20))
        box_sw_items_group.add(sw_win, sw_linux, sw_mac)


        # 图标创建按钮
        button_create_icon = Button(text='生成icon', on_press=self.btn_create_icon_handler)

        # 说明
        label_tips = Label(textwrap.dedent('说明:\
                                \n1. 图标包含尺寸24, 32, 48, 64, 96, 128\
                                \n2. win->.ico, linux->png套图, mac->.icns\
                                \n3. 程序自动将原图扩展为正方形,透明填充\
                                \n4. 原图边长至少>=128,否则将无法匹配完整尺寸\
                                '))

        # 组装
        box_main = Box(style=Pack(direction=COLUMN, align_items=CENTER, gap=10, margin=10))
        box_main.add(box_from_group, divider1, box_sw_items_group, divider2, button_create_icon, label_tips)

        self.main_window = MainWindow(title='图片转图标(.ico .png .icns)')
        self.main_window.size = 350, 200
        self.main_window.position = (self.screens[0].size[0] - 350) // 2, (self.screens[0].size[1] - 200) // 2 - 150

        self.main_window.content = box_main
        self.main_window.show()
        

    async def on_button_select_file(self, widget: Button):
        '''
        选择原图文件并得到路径
        '''
        self.entry_img_from.value = await self.main_window.dialog(OpenFileDialog('选择源文件', file_types=['png', 'tiff', 'webp', 'bmp', 'jpg', 'jpeg'])) # 返回值是None或者windowspath对象, entry会自动转义

    async def btn_create_icon_handler(self, widget: Button):
        '''
        生成ico
        '''
        # 获取原图地址
        if p := self.entry_img_from.value:
            path_from = p
        else:
            await self.main_window.dialog(InfoDialog('找不到原图', '必须指定一个原图地址'))
            return
        
        # 获取输出选项
        output_platform: list[str] = []
        for sw in self.g_sw_itmes_widgets:
            if sw.value is True:
                output_platform.append(sw.id)
            
        if not output_platform:
            await self.main_window.dialog(InfoDialog('未选择输出项', '至少选择一个平台作为输出项'))
            return
        

        # 弹出保存窗口供用户选择
        path_user = await self.main_window.dialog(SelectFolderDialog('icon另存为'))
        if not path_user:
            return
        
        
        # 以时间戳为名创建一个单独的文件夹作为根目录,按需创建子文件夹
        new_folder_name = str(int(time.time()))
        os.mkdir(os.path.join(path_user, new_folder_name))
        root_dir = os.path.join(path_user, new_folder_name)

        if self.g_sw_itmes_widgets[0].value is True:
            win_dir = os.path.join(root_dir, 'win')
            os.mkdir(win_dir)

        if self.g_sw_itmes_widgets[1].value is True:
            linux_dir = os.path.join(root_dir, 'linux')
            os.mkdir(linux_dir)

        if self.g_sw_itmes_widgets[2].value is True:
            mac_dir = os.path.join(root_dir, 'mac')
            os.mkdir(mac_dir)   

        # 处理并按需保存图片
        with Image.open(path_from) as f:
            # 如果非正方形,先把原图整成正方形
            width, height = f.size
            if width != height: 
                max_size = max(width, height)
                new_img = Image.new('RGBA', (max_size, max_size), (0, 0, 0, 0))
                left = (max_size - width) // 2
                top = (max_size - height) // 2
                new_img.paste(f, (left, top))
                f = new_img

            # 过滤掉无法生成的尺寸
            warning_text = None
            sizes_ok = list(filter(lambda x: f.size[0] >= x, self.g_ICON_SIZE))
            if not sizes_ok:
                await self.main_window.dialog(InfoDialog('无法生成icon', '原图尺寸太小,无法生成图标!'))
                return
            
            sizes_null = list(filter(lambda x: f.size[0] < x, self.g_ICON_SIZE))
            if sizes_null:
                warning_text = '因原图过小(小于128), 仅生成{}, 未生成{}'.format(sizes_ok, sizes_null)

            # 按平台开始生成图标
            if self.g_sw_itmes_widgets[0].value is True: # windows平台,生成单个ico
                f.save(os.path.join(win_dir, 'group-windows.ico'), 'ICO', sizes=list(map(lambda x: (x, x), sizes_ok)))

            if self.g_sw_itmes_widgets[1].value is True: # linux平台,生成png套图
                for length in sizes_ok:
                    resized_img = f.resize([length, length])
                    resized_img.save(os.path.join(linux_dir, f'group-linux-{length}.png'), 'PNG')

            if self.g_sw_itmes_widgets[2].value is True: # mac平台,生成单位icns
                f.save(os.path.join(mac_dir, 'group-mac.icns'), 'ICNS', sizes=list(map(lambda x: (x, x), sizes_ok)))

        # 告知用户有未成功生成的尺寸(如果有)
        if warning_text is not None:
            with open(os.path.join(root_dir, '警告.txt'), 'w') as f:
                f.write(warning_text)

        ret = await self.main_window.dialog(QuestionDialog('转换已完成', 'icon已生成, 按<是>打开目标文件夹'))
        if ret is True:
            match sys.platform:
                case 'win32':
                    os.startfile(root_dir)
                case 'linux':
                    subprocess.run(['xdg-open', root_dir])
    
def main():
    os.chdir(os.path.dirname(__file__))
    return MainForm('img2ico', 
                    'com.cccovs.img2ico', 
                    icon=os.path.abspath('./bin/icons/group'), 
                    author='cccovs',
                    version='0.0.2',
                    description='将普通图片转换成win,linux,mac平台icon',
                    home_page='https://gitcode.com/cccovs/img2ico'
                    )

if __name__ == '__main__':
    main().main_loop()
