![win_platform](resources/win_platform.png)

<font class="text-color-10" color="#4caf50">项目介绍</font><br>
将png, tiff, webp, bmp, jpg, jpeg图片格式快速转为ico, icns, png, Windows, MacOS,Linux推荐的图标文件(icon)<br><br>
  
<font class="text-color-10" color="#4caf50">依赖库</font><br>
toga<br>
pillow<br>

<font class="text-color-10" color="#4caf50">开发平台</font><br>
windows 11<br>
ubuntu 24.04<br>

<font class="text-color-10" color="#4caf50">下载</font><br>
[查看版本发布](https://gitcode.com/cccovs/img2ico/releases)